# language: pt
Funcionalidade: Matrícula de alunos em ofertas de disciplinas

  Cenário de Fundo:
    Dado um aluno chamado "Stringulino dos Floats"

  # TODO
  # tags @
  Delineação do Cenário: Um aluno se matricula com sucesso em uma disciplina ofertada
    E uma disciplina chamada "<Disciplina>"
    E que ainda há <Vagas antes> vagas na oferta da disciplina "<Disciplina>"

    Quando o aluno "Stringulino dos Floats" se matricular na oferta da disciplina "<Disciplina>"

    Então a quantidade de vagas na oferta da disciplina "<Disciplina>" passa a ser <Vagas depois>
    E o aluno "Stringulino dos Floats" está matriculado na oferta da disciplina "<Disciplina>"

    Exemplos:
      | Disciplina                       | Vagas antes | Vagas depois |
      | Tópicos Avançados em Programação | 20          | 19           |
      | Banco de Dados                   | 5           | 4            |
      | Matemática Discreta              | 1           | 0            |

  @persistence
  Cenário: Um aluno não pode matricular-se em mais que 6 ofertas de disciplinas em um mesmo semestre
    E as ofertas das disciplinas
      | Disciplina                       | Carga Horária | Semestre | Vagas |
      | Introdução à Programação         | 30            | 2020.2   | 20    |
      | Orientação a Objetos 1           | 60            | 2020.2   | 20    |
      | Orientação a Objetos 2           | 60            | 2020.2   | 30    |
      | Estrutrura de Dados              | 60            | 2020.2   | 10    |
      | Tópicos Avançados em Programação | 40            | 2020.2   | 30    |
      | Metodogia Científica             | 45            | 2020.2   | 30    |
      | Banco de Dados 1                 | 120           | 2020.2   | 30    |

    # TODO
    E o aluno "Stringulino dos Floats" se matricular nas ofertas das disciplinas
      | Disciplina                       |
      | Introdução à Progração           |
      | Orientação a Objetos 1           |
      | Orientação a Objetos 2           |
      | Estrutrura de Dados              |
      | Tópicos Avançados em Programação |
      | Metodogia Científica             |
    Quando o aluno "Stringulino dos Floats" se matricular na oferta da disciplina "Banco de Dados 1"
    # TODO
    Então o aluno "Stringulino dos Floats" não é matriculado na oferta da disciplina "Banco de Dados 1"
