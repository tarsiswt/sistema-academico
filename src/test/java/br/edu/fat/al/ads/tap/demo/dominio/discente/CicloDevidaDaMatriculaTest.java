package br.edu.fat.al.ads.tap.demo.dominio.discente;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.util.ReflectionTestUtils;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@DataJpaTest
class CicloDevidaDaMatriculaTest {

    @PersistenceContext
    EntityManager entityManager;

    @Test
    @DisplayName("Persistir aluno com matrícula cujo código é inválido resulta em exceção")
    public void persistirAlunoComMatriculaCujoCodigoEInvalidoResultaEmExcecao() {
        String codigoValido = "202012345";
        String codigoInvalido = "2020";
        Matricula matricula = new Matricula(codigoValido);
        ReflectionTestUtils.setField(matricula, "codigo", codigoInvalido);

        Aluno stringulino = new Aluno("Stringulino", matricula);

        Assertions.assertThrows(IllegalStateException.class, () -> entityManager.persist(stringulino));
    }

    @Test
    @DisplayName("Carregar aluno com matrícula inválida persitida resulta em exceção")
    public void f() {
        // Dado um aluno já persistido no banco de dados com uma matrícula inválida
        // Quando carregar este aluno
        // Então deve ser lançada uma exceção
    }
}
