package br.edu.fat.al.ads.tap.demo.cucumber.persistencia;

import br.edu.fat.al.ads.tap.demo.DemoApplication;
import io.cucumber.java.Before;
import org.springframework.boot.test.context.SpringBootTest;

import javax.transaction.Transactional;

@SpringBootTest(classes = DemoApplication.class)
public class CucumberContextConfiguration {

    @Before
    @Transactional
    public void setup_cucumber_spring_context() {

    }
}
