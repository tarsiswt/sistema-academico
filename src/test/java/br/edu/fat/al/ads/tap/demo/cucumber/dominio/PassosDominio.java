package br.edu.fat.al.ads.tap.demo.cucumber.dominio;

import br.edu.fat.al.ads.tap.demo.dominio.curriculo.CargaHoraria;
import br.edu.fat.al.ads.tap.demo.dominio.curriculo.Disciplina;
import br.edu.fat.al.ads.tap.demo.dominio.curriculo.OfertaDisciplina;
import br.edu.fat.al.ads.tap.demo.dominio.discente.Aluno;
import br.edu.fat.al.ads.tap.demo.dominio.discente.Matricula;
import io.cucumber.java.pt.Dado;
import io.cucumber.java.pt.E;
import io.cucumber.java.pt.Então;
import io.cucumber.java.pt.Quando;

public class PassosDominio {

    private Aluno aluno;
    private Disciplina disciplina;
    private OfertaDisciplina oferta;
    private int quantidadeDeVagasAntesDaMatricula;

    @Dado("um aluno chamado {string} com a matrícula {string}")
    public void umAlunoChamado(String nomeDoAlunoNoCenario, String matriculaDoAlunoNoCenario) {
        this.aluno = new Aluno(nomeDoAlunoNoCenario, new Matricula(matriculaDoAlunoNoCenario));
    }

    @Dado("um aluno chamado {string}")
    public void umAlunoChamado(String nomeDoAlunoNoCenario) {
        this.aluno = new Aluno(nomeDoAlunoNoCenario, new Matricula("202012345"));
    }

    @E("uma disciplina chamada {string}")
    public void umaDisciplinaChamada(String nomeDaDisciplinaNoCenario) {
        this.disciplina = new Disciplina(nomeDaDisciplinaNoCenario, new CargaHoraria(60));
    }

    @E("que ainda há vagas na oferta oferta da disciplina {string}")
    public void queAindaHáVagasNaOfertaOfertaDaDisciplina(String nomeDaDisciplinaNoCenario) {
        this.oferta = new OfertaDisciplina("2020.2", this.disciplina, 20);
        assert this.oferta.haVagas();
    }

    @Quando("o aluno {string} se matricular na oferta da disciplina {string}")
    public void oAlunoSeMatricularNaOfertaDaDisciplina(String nomeDoAlunoNoCenario, String nomeDaDisciplinaNoCenario) {
        this.quantidadeDeVagasAntesDaMatricula = this.oferta.quantidadeDeVagasDisponiveis();
        this.oferta.matricular(this.aluno.getMatricula());
    }

    @Então("a quantidade de vagas na oferta da disciplina {string} diminui em {int}")
    public void aQuantidadeDeVagasNaOfertaDaDisciplinaDiminuiEm(String nomeDaDisciplina, int quantidadeDeVagasParaDiminuir) {
        assert this.quantidadeDeVagasAntesDaMatricula ==
               this.oferta.quantidadeDeVagasDisponiveis() + quantidadeDeVagasParaDiminuir;

        assert this.oferta.quantidadeDeVagasDisponiveis() ==
               this.quantidadeDeVagasAntesDaMatricula - quantidadeDeVagasParaDiminuir;
    }
}
