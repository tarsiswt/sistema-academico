package br.edu.fat.al.ads.tap.demo.aplicacao.curriculo;

import br.edu.fat.al.ads.tap.demo.dominio.discente.Aluno;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class MatriculaServiceTransactionalTest {
    @Autowired
    MatriculaServiceTransactional matriculaServiceTransactional;

    @Test
    public void f() {
        Aluno stringulino = new Aluno("Stringulino", null);
        stringulino.setCpf("00000000000");
        String email = matriculaServiceTransactional.obterEmailDoAluno(stringulino);
        System.out.println(email);
    }

}
