package br.edu.fat.al.ads.tap.demo.aplicacao.discente;

import br.edu.fat.al.ads.tap.demo.dominio.discente.Matricula;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class GeradorDeMatriculaJPATest {

    @Autowired
    GeradorDeMatriculaJPA geradorDeMatriculaJPA;

    @Test
    @DisplayName("Uma matrícula é gerada com sucesso")
    public void umaMatriculaEGeradaComSuceso() {
        System.out.println(geradorDeMatriculaJPA.gerarNovaMatricula());
    }

    @Test
    @DisplayName("Duas matrículas geradas em sucessão são diferentes")
    public void duasMatriculasGeradasEmSucessaoSaoDiferentes() {
        Matricula primeiraMatricula = geradorDeMatriculaJPA.gerarNovaMatricula();
        Matricula segundaMatricula = geradorDeMatriculaJPA.gerarNovaMatricula();

        Assertions.assertNotEquals(primeiraMatricula, segundaMatricula);
    }

}
