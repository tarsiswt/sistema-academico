package br.edu.fat.al.ads.tap.demo.cucumber.persistencia;

import br.edu.fat.al.ads.tap.demo.aplicacao.curriculo.DisciplinaRepository;
import br.edu.fat.al.ads.tap.demo.aplicacao.curriculo.OfertaDisciplinaRepository;
import br.edu.fat.al.ads.tap.demo.aplicacao.discente.AlunoRepository;
import br.edu.fat.al.ads.tap.demo.dominio.curriculo.CargaHoraria;
import br.edu.fat.al.ads.tap.demo.dominio.curriculo.Disciplina;
import br.edu.fat.al.ads.tap.demo.dominio.curriculo.OfertaDisciplina;
import br.edu.fat.al.ads.tap.demo.dominio.discente.Aluno;
import br.edu.fat.al.ads.tap.demo.dominio.discente.Matricula;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.Before;
import io.cucumber.java.pt.Dado;
import io.cucumber.java.pt.E;
import io.cucumber.java.pt.Então;
import io.cucumber.java.pt.Quando;
import org.springframework.beans.factory.annotation.Autowired;

import javax.transaction.Transactional;
import java.util.*;

@Transactional
public class PassosComPersistencia {

    @Autowired
    OfertaDisciplinaRepository ofertas;

    @Autowired
    AlunoRepository alunos;

    @Autowired
    DisciplinaRepository disciplinas;

    @Before
    public void antesDeCadaCenario() {
        alunos.deleteAll();
        ofertas.deleteAll();
        disciplinas.deleteAll();
    }

    @Dado("um aluno chamado {string}")
    public void umAlunoChamado(String nome) {
        alunos.save(new Aluno(nome, new Matricula("202012345")));
    }

    @E("uma disciplina chamada {string}")
    public void umaDisciplina(String nomeDaDisciplina) {
        Disciplina disciplina = new Disciplina(nomeDaDisciplina, new CargaHoraria(60));
        this.disciplinas.save(disciplina);
    }

    @E("que ainda há {int} vagas na oferta da disciplina {string}")
    public void haVagas(int quantidadeDeVagasRestantesNoCenario, String nomeDaDisciplina) {
        List<Disciplina> disciplinasComONomeDoCenario = disciplinas.procurarDisciplinaPeloNome(nomeDaDisciplina);
        assert disciplinasComONomeDoCenario.size() == 1;
        OfertaDisciplina ofertaDisciplina = new OfertaDisciplina("2020.2", disciplinasComONomeDoCenario.get(0), quantidadeDeVagasRestantesNoCenario);
        ofertas.save(ofertaDisciplina);
        assert ofertaDisciplina.haVagas();
    }

    @Quando("o aluno {string} se matricular na oferta da disciplina {string}")
    public void seMatricula(String nomeDoAluno, String nomeDaDisciplina) {
        Aluno aluno = buscarAlunoPeloNome(nomeDoAluno);

        OfertaDisciplina ofertaDisciplina = buscarOfertaDisciplinaPeloNomeDaDisciplina(nomeDaDisciplina);

        ofertaDisciplina.matricular(aluno.getMatricula());
        ofertas.save(ofertaDisciplina);
    }


    @Então("a quantidade de vagas na oferta da disciplina {string} passa a ser {int}")
    public void quantidadeDeVagas(String nomeDaDisciplinaNoCenario, int quantidadeDeVagasRestantesNoCenario) {
        OfertaDisciplina ofertaDisciplina = buscarOfertaDisciplinaPeloNomeDaDisciplina(nomeDaDisciplinaNoCenario);
        assert ofertaDisciplina.quantidadeDeVagasDisponiveis() == quantidadeDeVagasRestantesNoCenario;
    }

    @E("o aluno {string} está matriculado na oferta da disciplina {string}")
    public void oAlunoEstáMatriculadoNaOfertaDaDisciplina(String nomeDoAlunoNoCenario, String nomeDaDisciplinaNoCenario) {
        Aluno alunoDoCenario = buscarAlunoPeloNome(nomeDoAlunoNoCenario);
        OfertaDisciplina ofertaDisciplinaDoCenario = buscarOfertaDisciplinaPeloNomeDaDisciplina(nomeDaDisciplinaNoCenario);

        Set<Matricula> matriculas = ofertaDisciplinaDoCenario.matriculasDoAlunos();
        assert matriculas.contains(alunoDoCenario.getMatricula());
    }

    @E("as ofertas das disciplinas")
    public void asOfertasDasDisciplinas(DataTable tabela) {
        List<OfertaDisciplina> ofertasDoCenario = transformarDeTabelaEmOfertasDisciplinas(tabela);
        for (OfertaDisciplina ofertaDoCenario : ofertasDoCenario) {
            disciplinas.save(ofertaDoCenario.getDisciplina());
            ofertas.save(ofertaDoCenario);
        }
    }

    private List<OfertaDisciplina> transformarDeTabelaEmOfertasDisciplinas(DataTable tabela) {
        ArrayList<OfertaDisciplina> ofertasDisciplinasDaTabela = new ArrayList<>();
        List<Map<String, String>> maps = tabela.asMaps();
        for (Map<String, String> linha : maps) {
            Disciplina disciplina = construirDisciplinaAPartirDeLinha(linha);
            OfertaDisciplina ofertaDisciplina = construirConstruirOfertaDisciplinaAPartirDeLinhaEDisciplina(disciplina, linha);
            ofertasDisciplinasDaTabela.add(ofertaDisciplina);
        }
        return ofertasDisciplinasDaTabela;
    }

    private OfertaDisciplina construirConstruirOfertaDisciplinaAPartirDeLinhaEDisciplina(Disciplina disciplina, Map<String, String> linha) {
        return new OfertaDisciplina(linha.get("Semestre"), disciplina, Integer.parseInt(linha.get("Vagas")));
    }

    private Disciplina construirDisciplinaAPartirDeLinha(Map<String, String> linha) {
        return new Disciplina(linha.get("Disciplina"), new CargaHoraria(Integer.parseInt(linha.get("Carga Horária"))));
    }

    private Aluno buscarAlunoPeloNome(String nomeDoAluno) {
        List<Aluno> alunoComONomeDoCenario = alunos.procurarAlunoPeloNome(nomeDoAluno);
        assert alunoComONomeDoCenario.size() == 1;
        return alunoComONomeDoCenario.get(0);
    }

    private OfertaDisciplina buscarOfertaDisciplinaPeloNomeDaDisciplina(String nomeDaDisciplina) {
        Optional<OfertaDisciplina> possivelOfertaDisciplina = ofertas.ofertasDeDisciplinasParaOSemestre("2020.2")
                .stream()
                .filter(ofertaDisciplina -> ofertaDisciplina.getDisciplina().getNome().equals(nomeDaDisciplina))
                .findFirst();
        assert possivelOfertaDisciplina.isPresent();

        return possivelOfertaDisciplina.get();
    }
}
