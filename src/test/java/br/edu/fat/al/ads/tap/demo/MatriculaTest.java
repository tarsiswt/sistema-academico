package br.edu.fat.al.ads.tap.demo;

import br.edu.fat.al.ads.tap.demo.dominio.discente.Matricula;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class MatriculaTest {

    @Test
    public void duasMatriculasSaoIguaisSeSeusCodigosSao() {
        assert new Matricula("202012345").equals(new Matricula("202012345"));
        Assertions.assertNotEquals(new Matricula("202012345"), new Matricula("202054321"));
    }

    @Test
    public void umaMatriculaComOCodigo202012345ÉCriadoComSucesso() {
        new Matricula("202012345");
    }

    @Test
    public void umaMatriculaComCaracteresEspeciaisNãoDeveSerCriadaComSucesso() {
        try {
            new Matricula("20201234#");
            assert false;
        } catch (RuntimeException ex) {
            // conforme esperado
        }
    }

    @Test
    public void umaMatriculaComLetrasNãoDeveSerCriadaComSucesso() {
        try {
            new Matricula("2O2012345");
            assert false;
        } catch (RuntimeException ex) {
            // conforme esperado
        }
    }

    @Test
    public void umaMatriculaCom10DígitosNãoDeveSerCriadaComSucesso() {
        try {
            new Matricula("2020123456");
            assert false;
        } catch (RuntimeException ex) {
            // conforme esperado
        }
    }
}
