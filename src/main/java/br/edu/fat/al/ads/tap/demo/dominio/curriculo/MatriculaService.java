package br.edu.fat.al.ads.tap.demo.dominio.curriculo;

import br.edu.fat.al.ads.tap.demo.dominio.discente.Aluno;

import java.util.Collection;

public interface MatriculaService {
    void matricularAlunoEmDisciplinas(Aluno aluno, Collection<OfertaDisciplina> ofertas);
}
