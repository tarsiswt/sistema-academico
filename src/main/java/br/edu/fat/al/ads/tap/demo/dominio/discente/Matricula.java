package br.edu.fat.al.ads.tap.demo.dominio.discente;

import com.google.common.base.Preconditions;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.regex.Pattern;

/**
 * Value Object (Objeto de Valor) que representa a matrícula de um Aluno e segue o padrão AAAADDDDD
 */
@Embeddable
@EqualsAndHashCode
@ToString
public class Matricula implements Serializable {
    static Pattern padraoDaMatricula = Pattern.compile("\\d{4}\\d{5}");

    @Getter
    String codigo;

    protected Matricula() {
    }

    public Matricula(String codigo) {
        Preconditions.checkArgument(queOCodigoCasaComOPadrao(codigo));
        this.codigo = codigo;
    }

    public boolean queOCodigoCasaComOPadrao(String codigo) {
        return padraoDaMatricula.matcher(codigo).matches();
    }

    public boolean queOCodigoCasaComOPadrao() {
        return padraoDaMatricula.matcher(codigo).matches();
    }
}
