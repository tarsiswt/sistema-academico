package br.edu.fat.al.ads.tap.demo.aplicacao.discente;

import br.edu.fat.al.ads.tap.demo.dominio.discente.GeradorDeMatricula;
import br.edu.fat.al.ads.tap.demo.dominio.discente.Matricula;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.time.LocalDateTime;

@Service
public class GeradorDeMatriculaJPA implements GeradorDeMatricula {

    @PersistenceContext
    EntityManager entityManager;

    @Override
    public Matricula gerarNovaMatricula() {
        int anoAtual = LocalDateTime.now().getYear();
        int digitosFinais = gerarDigitosFinais();
        String digitosFinaisPreenchidos = preencherComZerosAEsquerda(digitosFinais);
        return new Matricula(anoAtual + digitosFinaisPreenchidos);
    }

    private String preencherComZerosAEsquerda(int digitosFinais) {
        return String.format("%05d", digitosFinais);
    }

    private int gerarDigitosFinais() {
        // TODO: otimizar a inicialização e reuso do entityManager
        Query consultaPeloProximoValor = entityManager.createNativeQuery("select matricula_seq.nextval from dual");
        Object valorDaSequence = consultaPeloProximoValor.getSingleResult();
        return Integer.valueOf(String.valueOf(valorDaSequence));
    }
}
