package br.edu.fat.al.ads.tap.demo.aplicacao.curriculo;

import br.edu.fat.al.ads.tap.demo.dominio.curriculo.Disciplina;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DisciplinaRepository extends CrudRepository<Disciplina, Long> {
    @Query("select d from Disciplina d where d.nome = :nome")
    List<Disciplina> procurarDisciplinaPeloNome(@Param("nome") String nomeDaDisciplina);
}
