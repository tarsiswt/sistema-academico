package br.edu.fat.al.ads.tap.demo.aplicacao.curriculo;

import br.edu.fat.al.ads.tap.demo.dominio.curriculo.OfertaDisciplina;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.Set;

@org.springframework.stereotype.Repository
public interface OfertaDisciplinaRepository extends CrudRepository<OfertaDisciplina, Long> {

    @Query("select od from OfertaDisciplina od where od.semestre = :semestre")
    Set<OfertaDisciplina> ofertasDeDisciplinasParaOSemestre(@Param("semestre") String semestre);
}
