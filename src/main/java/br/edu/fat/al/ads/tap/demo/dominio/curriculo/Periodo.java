package br.edu.fat.al.ads.tap.demo.dominio.curriculo;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.io.Serializable;
import java.util.Set;

@Entity
public class Periodo implements Serializable {
    @Id
    @GeneratedValue
    Long id;
    String nome;
    //TODO: Faz sentido ordenar por horário?
    @OneToMany
    Set<Disciplina> disciplina;
}
