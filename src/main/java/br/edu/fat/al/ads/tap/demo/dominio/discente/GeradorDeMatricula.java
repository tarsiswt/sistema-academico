package br.edu.fat.al.ads.tap.demo.dominio.discente;

public interface GeradorDeMatricula {
    Matricula gerarNovaMatricula();
}
