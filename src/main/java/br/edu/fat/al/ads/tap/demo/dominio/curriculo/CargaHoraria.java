package br.edu.fat.al.ads.tap.demo.dominio.curriculo;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class CargaHoraria implements Serializable {
    int cargaHoraria;

    public CargaHoraria(int cargaHoraria) {
        // TODO: refatorar a regra de validação para o outro construtor
        if (cargaHoraria <= 0) {
            throw new RuntimeException("A carga horária deve ser maior ou igual a zero");
        }
        this.cargaHoraria = cargaHoraria;
    }

    protected CargaHoraria() {
    }
}
