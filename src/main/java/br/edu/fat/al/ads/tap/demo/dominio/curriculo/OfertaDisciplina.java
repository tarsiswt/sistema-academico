package br.edu.fat.al.ads.tap.demo.dominio.curriculo;

import br.edu.fat.al.ads.tap.demo.dominio.discente.Matricula;
import lombok.Getter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
public class OfertaDisciplina implements Serializable {
    @Id
    @GeneratedValue
    Long id;
    // TODO: falta capturar e modelar o conceito abaixo
    String semestre;
    @OneToOne
    @Getter
    Disciplina disciplina;
    // TODO: considerar modelar o limite como um objeto de valor e ponderar contra a proliferação de tipos
    int limiteDeMatriculados = 20;
    @ElementCollection
    @CollectionTable
    Set<Matricula> matriculas = new HashSet<>();

    public OfertaDisciplina(String semestre, Disciplina disciplina, int limiteDeMatriculados) {
        this.semestre = semestre;
        this.disciplina = disciplina;
        this.limiteDeMatriculados = limiteDeMatriculados;
    }

    protected OfertaDisciplina() {
    }

    public void matricular(Matricula matricula) {
        if (haVagas()) {
            matriculas.add(matricula);
        } else {
            // TODO: criar tipo específico para esta situação (throw new LimiteDeMatriculadosException)
            throw new RuntimeException("O limite de matriculados na disciplina foi atingido");
        }
    }

    public boolean haVagas() {
        return matriculas.size() < limiteDeMatriculados;
    }

    public int quantidadeDeVagasDisponiveis() {
        return limiteDeMatriculados - matriculas.size();
    }

    public Set<Matricula> matriculasDoAlunos() {
        return new HashSet<>(matriculas);
    }
}
