package br.edu.fat.al.ads.tap.demo.aplicacao.curriculo;

import br.edu.fat.al.ads.tap.demo.dominio.curriculo.MatriculaService;
import br.edu.fat.al.ads.tap.demo.dominio.curriculo.OfertaDisciplina;
import br.edu.fat.al.ads.tap.demo.dominio.discente.Aluno;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.transaction.Transactional;
import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class MatriculaServiceTransactional implements MatriculaService {

    @Autowired
    OfertaDisciplinaRepository ofertaDisciplinaRepository;

    @Override
    @Transactional
    public void matricularAlunoEmDisciplinas(Aluno aluno, Collection<OfertaDisciplina> ofertas) {
        for (OfertaDisciplina oferta : ofertas) {
            oferta.matricular(aluno.getMatricula());
        }
        // TODO: enviar e-mail ao aluno com as informações das matrículas
        String email = obterEmailDoAluno(aluno);
        enviarEmailComInformacoesDaMatricula(email);
    }

    private void enviarEmailComInformacoesDaMatricula(String email) {
        // TODO: implementar
    }

    public String obterEmailDoAluno(Aluno aluno) {
        AlunoDTO dtoPessoaFisica = new RestTemplate()
                .getForObject("http://localhost:8081/pessoa-fisica/search/findByCPF?cpf={cpf}", AlunoDTO.class, aluno.getCpf());
        return dtoPessoaFisica.getEmail();
    }

    public Set<OfertaDisciplina> ofertasDeDisciplinasComVagasDisponiveis(String semestre) {
        // 3
        return ofertaDisciplinaRepository.ofertasDeDisciplinasParaOSemestre(semestre)
                .stream()
                .filter(OfertaDisciplina::haVagas)
                .collect(Collectors.toSet());
        // 2
//        Set<OfertaDisciplina> ofertaDisciplinas =
//                ofertaDisciplinaRepository.ofertasDeDisciplinasParaOSemestre(semestre);
//        ofertaDisciplinas.removeIf(oferta -> !oferta.haVagas());
//        return ofertaDisciplinas;
        // 1
//        Set<OfertaDisciplina> ofertaDisciplinas =
//                ofertaDisciplinaRepository.ofertasDeDisciplinasParaOSemestre(semestre);
//        Set apenasOfertasComVagas = new HashSet<OfertaDisciplina>();
//        for (OfertaDisciplina ofertaDisciplina : ofertaDisciplinas) {
//            if (ofertaDisciplina.haVagas()) {
//                apenasOfertasComVagas.addAll(ofertaDisciplinas);
//            }
//        }
//        return apenasOfertasComVagas;
    }

    @Data
    private static class AlunoDTO {
        String nome;
        String cpf;
        String email;
    }
}
