package br.edu.fat.al.ads.tap.demo.dominio.curriculo;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
public class Curso implements Serializable {
    @Id
    @GeneratedValue
    Long id;
    String nome;
    @Enumerated
    Turno turno;
    @OneToMany
    List<Periodo> periodos;
}
