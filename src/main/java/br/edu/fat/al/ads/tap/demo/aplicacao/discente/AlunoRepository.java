package br.edu.fat.al.ads.tap.demo.aplicacao.discente;

import br.edu.fat.al.ads.tap.demo.dominio.discente.Aluno;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AlunoRepository extends CrudRepository<Aluno, Long> {
    @Query(value = "select aluno from Aluno aluno where aluno.nome = :nome")
    List<Aluno> procurarAlunoPeloNome(@Param("nome") String nomeDoAluno);
}
