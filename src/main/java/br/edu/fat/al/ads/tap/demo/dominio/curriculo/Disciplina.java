package br.edu.fat.al.ads.tap.demo.dominio.curriculo;

import lombok.Getter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.io.Serializable;

@Entity
public class Disciplina implements Serializable {
    @Id
    @GeneratedValue
    Long id;
    @Getter
    String nome;
    CargaHoraria cargaHoraria;

    public Disciplina(String nome, CargaHoraria cargaHoraria) {
        this.nome = nome;
        this.cargaHoraria = cargaHoraria;
    }

    protected Disciplina() {
    }
}
