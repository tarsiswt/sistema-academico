package br.edu.fat.al.ads.tap.demo.dominio.curriculo;

import java.io.Serializable;

public enum Turno implements Serializable {
    MATUTINO, VESPERTINO, NOTURNO
}
