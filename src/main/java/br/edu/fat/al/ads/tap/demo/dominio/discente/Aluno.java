package br.edu.fat.al.ads.tap.demo.dominio.discente;

import br.edu.fat.al.ads.tap.demo.dominio.curriculo.Curso;
import br.edu.fat.al.ads.tap.demo.dominio.curriculo.OfertaDisciplina;
import com.google.common.base.Preconditions;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

// Alt. 1
// Contexto             --(CPF)->           Contexto
// PessoaFisica -Upstream--->---Downstream- Academico
// Open Host System                         Consumidor

// Alt. 2
// Contexto                                 Contexto
// PessoaFisica  ----Parceria (UUID)----    Academico
// Open Host System                         Consumidor
@Entity
public class Aluno implements Serializable {
    @Id
    @GeneratedValue
    Long id;
    String nome;
    @Getter
    Matricula matricula;
    // Alt. 1
    @Setter
    @Getter
    String cpf;

    @ManyToMany
    Set<Curso> cursos;

    public Aluno() {
    }

    public Aluno(String nome, Matricula matricula) {
        this.nome = nome;
        this.matricula = matricula;
    }

    @PostLoad
    @PrePersist
    private void validaCodigoDaMatricula() {
        Preconditions.checkState(matricula.queOCodigoCasaComOPadrao());
    }

    public void matricularNoCurso(Curso curso) {
        this.cursos.add(curso);
    }

    public void matricularNaDisciplina(OfertaDisciplina ofertaDisciplina) {
        ofertaDisciplina.matricular(this.matricula);
    }
}
